document.getElementById('form').addEventListener('submit', function(e) {
	e.preventDefault();
});
function validateForm() {
	var name = document.getElementById('name');

	if (name.value == '') {
		alert('Please enter your name');
		name.focus();
		return false;
	} else {
		var regExpName = /^[A-Z][a-z]+\s[A-Z][a-z]+$/;
		if (!regExpName.test(name.value)) {
			alert('Please enter name in correct format');
			name.focus();
			return false;
		}
	}
	var email = document.getElementById('email');
	if (email.value == '') {
		alert('Please enter your email');
		email.focus();
		return false;
	} else {
		var regExpEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		if (!regExpEmail.test(email.value)) {
			alert('Email is not in correct format');
			email.focus();
			return false;
		}
	}
	var phoneNum = document.getElementById('phone-number');
	if (phoneNum.value == '') {
		alert('Please enter your phone');
		phoneNum.focus();
		return false;
	} else {
		var regExpPhone = /^06[0-9]-[0-9]{3}-[0-9]{3,4}$/;
		if (!regExpPhone.test(phoneNum.value)) {
			alert('Phone is not in correct format');
			phoneNum.focus();
			return false;
		}
	}
	alert('Your submission was sucsessful! You will be redirected to home page.');
	window.location.href = '../index.html';
}
