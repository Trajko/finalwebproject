let api =
	'https://en.wikipedia.org/w/api.php?action=query&prop=pageimages%7Cextracts&exintro&explaintext&exlimit=max&format=json&titles=';
let searchBar = document.getElementById('user-search-input');
let query = searchBar.value;
let goButton = document.getElementById('search-button');
let url = '';
let genDiv = document.getElementById('generated-div');
var imgApi = 'https://source.unsplash.com/1600x900/?';
var imgUrl = '';

goButton.addEventListener('click', function(e) {
	// Listen for click on search button

	$('#generated-div').empty(); // Clear search results

	e.preventDefault(); // Prevent default behavior of submit button

	if (searchBar.value === '') {
		// If search bar is empty
		alert('This can not be empty');
	} else {
		let apiUrl = api + searchBar.value.replace(' ', '%20'); // Replace whitespaces with %20 (represents space in url)
		imgUrl = '';
		imgUrl = imgApi + searchBar.value;
		// console.log(apiUrl);
		console.log(imgUrl);
		searchBar.value = ''; // Clear search bar
		url = apiUrl; // Set url to apiUrl

		searchDestination(apiUrl, imgUrl); // Call searchResults, passing in the apiUrl
	}
});

function searchDestination(url, imgUrl) {
	genDiv.innerHTML = '';
	$.ajax({
		method: 'GET', //simple get request
		url: url, // dynamic link
		dataType: 'jsonp', //
		contentType: 'application/json', //    essencially same as getting json response but uses jsonp and some parametars to bypass cors
		crossDomain: true, //
		success: function(result) {
			console.log(result); //debugging

			for (let i in result.query.pages) {
				genDiv.innerHTML += `
                    <div class="generated-jumbotron jumbotron" style="background-image: url('${imgUrl}');">
                    <h1 class="display-4">${result.query.pages[i].title}</h1>
                    <p class="lead">${result.query.pages[i].extract}</p>
            
           
                    <a href="form.html"><button type="button" class="btn btn-primary btn-bookit">Book it</button></a>
                    </div>
                    `;
			}
		}
	});
	//https://en.wikipedia.org/w/api.php?action=query&prop=pageimages%7Cextracts&exintro&explaintext&exlimit=max&format=json&titles=
}
